<?php
session_start();
use \App\Imon\Registration\Registration;
include_once ($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."My_First_Project".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");

$users = new Registration();
if($users->GetSession()){
    header("location:index.php");
    die();
}

if($_SERVER["REQUEST_METHOD"] == "POST"){
    $email = $_POST['email'];
    $password = $_POST['password'];
    if (!empty($email) && !empty($password)){
        
        $login = $users->login($email, $password);
        if($login){
            header("location:index.php");
        }  else {
            echo "<script>alert('email or password not matched!')</script>";
        }
        
    }else {
        echo "<script>alert('Any of the field cannot be empty!')</script>";
    } 
    
    
}

?>


<!DOCTYPE hml>
<html>
    <head>
        <title>Login Form</title>
        <link rel="stylesheet" type="text/css" href="../../../css/bootstrap-theme.css">
        <link rel="stylesheet" type="text/css" href="../../../css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../../../css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../../../css/bootstrap-theme.min.css">
        <style>
            body{
                
                background-image: url("../../../themes.jpg");
                background-size: 100%;
            }
        </style>
    </head>
    <body>
        <form class="form-group" action="" method="post">
         
            <div class="col-md-12" style="padding-top: 5%;">
                <div class="col-md-offset-4 col-md-2">
                    <label>Email Address</label>
                </div>
                <div class="col-md-2">
                    <input type="email" class="form-control" name="email" placeholder="example@examp.com"/>
                </div>
            </div>
            <div class="col-md-12" style="padding-top: 2%;">
                <div class="col-md-offset-4 col-md-2">
                    <label>Password</label>
                </div>
                <div class="col-md-2">
                    <input type="password" class="form-control" name="password" placeholder="give the password"/>
                </div>
            </div>
            
            <div class="col-md-12" style="padding-top: 2%;">
                <div class="col-md-offset-4 col-md-4">
                    <input type="submit" class="btn btn-primary btn-lg btn-block" value="Login" />
                </div>
            </div>
            
        </form>
            <div class="col-md-12" style="padding-top: 2%;">
                <div class="col-md-offset-4 col-md-4">
                    <a href="signup.php"><input type="submit" class="btn btn-primary btn-lg btn-block" value="Need to Registerd?" /></a>
                </div>
            </div>
    </body>
</html>

