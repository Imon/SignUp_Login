<?php
session_start();
use \App\Imon\Registration\Registration;
include_once ($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."My_First_Project".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");

$users = new Registration();
$username = $_SESSION['username'];
if (!$users->GetSession()){
header("location:../../../index.php");
die();
}
$user = $users->recycle();
?>
<!DOCTYPE hml>
<html>
    <head>
        <title>Home Page</title>
        <link rel="stylesheet" type="text/css" href="../../../css/bootstrap-theme.css">
        <link rel="stylesheet" type="text/css" href="../../../css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../../../css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../../../css/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="../../../css/StyleSheet.css">
        <link rel="stylesheet" type="text/css" href="../../../css/style.css">
        <style>
            body{
                
                background-image: url("../../../themes.jpg");
                background-size: 100%;
                padding-left: 15%; 
                padding-right: 15%;
            }
        </style>
        
    </head>
    <body>
        <h1>Hello world</h1>
        <span style="padding-left:80%;"><?php echo "hi ".$username." "?><a href="logout.php">Logout</a></span>
        <table border="1">
            <thead class="TabHead">
                <tr>
                    <td>SL.No.</td>
                    <td>Name</td>
                    <td>Date of Birth</td>
                    <td>Email</td>
                    <td style="text-align: center" colspan="5">Action</td>
                </tr>
            </thead>
            <tbody class="tabBody">
                  <?php
                    $i=0;
                    foreach ($user as $detail){
                        $i++;
                    ?>
                <tr>
                    <td><?php echo $i;?></td>
                    <td><?php echo $detail['name'];?></td>
                    <td><?php echo $detail['dobirth'];?></td>
                    <td><?php echo $detail['email'];?></td>
                    <td>View</td>
                    <td>
                        <form action="recover.php" method="post">
                            <input type="hidden" name="id" value="<?php echo $detail['id'];?>"/>
                            <input type="submit" value="Recover"/>
                        </form>
                    </td>
                    <td>
                        <form action="delete.php" method="post">
                            <input type="hidden" name="id" value="<?php echo $detail['id'];?>"/>
                            <input type="submit" value="Permanently Delete"/>
                        </form>
                    </td>
                    
                    <td>Email</td><br/>
                </tr>
                <?php
                    }
                ?>
            </tbody>
        </table>
        <h2><a href="index.php">Back to Home</a></h2>
        
    </body>
</html>


