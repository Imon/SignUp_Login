<?php

use \App\Imon\Registration\Registration;
include_once ($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."My_First_Project".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");

$id = $_POST['id'];
$users = new Registration();

$detail = $users->edit($id);
?>
<!DOCTYPE hml>
<html>
    <head>
        <title>Registration Form</title>
        <link rel="stylesheet" type="text/css" href="../../../css/bootstrap-theme.css">
        <link rel="stylesheet" type="text/css" href="../../../css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../../../css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../../../css/bootstrap-theme.min.css">
        <style>
            body{
                
                background-image: url("../../../themes.jpg");
                background-size: 100%;
            }
        </style>
    </head>
    <body>
        <form class="form-group" action="store.php" method="post">
            <input type="hidden" name="id" value="<?php echo $detail['id'];?>"/>
            <div class="col-md-12" style="padding-top: 5%;">
                <div class="col-md-offset-4 col-md-2">
                    <label>Full Name</label>
                </div>
                <div class="col-md-2">
                    <input type="text" class="form-control" name="name" value="<?php echo $detail['name'];?>"/>
                </div>
            </div>
            <div class="col-md-12" style="padding-top: 2%;">
                <div class="col-md-offset-4 col-md-2">
                    <label>User Name</label>
                </div>
                <div class="col-md-2">
                    <input type="text" class="form-control" name="user_name" value="<?php echo $detail['user_name'];?>"/>
                </div>
            </div>
            
            <div class="col-md-12" style="padding-top: 2%;">
                <div class="col-md-offset-4 col-md-2">
                    <label>Email Address</label>
                </div>
                <div class="col-md-2">
                    <input type="email" class="form-control" name="email" value="<?php echo $detail['email'];?>"/>
                </div>
            </div>
            
            <div class="col-md-12" style="padding-top: 2%;">
                <div class="col-md-offset-4 col-md-4">
                    <input type="submit" class="btn btn-primary btn-lg btn-block" value="Submit" />
                </div>
            </div>
            
        </form>
        
    </body>
</html>

