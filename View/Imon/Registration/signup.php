
<!DOCTYPE hml>
<html>
    <head>
        <title>Registration Form</title>
        <link rel="stylesheet" type="text/css" href="../../../css/bootstrap-theme.css">
        <link rel="stylesheet" type="text/css" href="../../../css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../../../css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../../../css/bootstrap-theme.min.css">
        <style>
            body{
                
                background-image: url("../../../themes.jpg");
                background-size: 100%;
            }
        </style>
    </head>
    <body>
        <form class="form-group" action="store.php" method="post">
            <div class="col-md-12" style="padding-top: 5%;">
                <div class="col-md-offset-4 col-md-2">
                    <label>Full Name</label>
                </div>
                <div class="col-md-2">
                    <input type="text" class="form-control" name="name" placeholder="Type your full name"/>
                </div>
            </div>
            <div class="col-md-12" style="padding-top: 2%;">
                <div class="col-md-offset-4 col-md-2">
                    <label>User Name</label>
                </div>
                <div class="col-md-2">
                    <input type="text" class="form-control" name="user_name" placeholder="Type the username"/>
                </div>
            </div>
            <div class="col-md-12" style="padding-top: 2%;">
                <div class="col-md-offset-4 col-md-2">
                    <label>Date of birth</label>
                </div>
                <div class="col-md-2">
                    <input type="date" class="form-control" name="dobirth"/>
                </div>
            </div>
            <div class="col-md-12" style="padding-top: 2%;">
                <div class="col-md-offset-4 col-md-2">
                    <label>Email Address</label>
                </div>
                <div class="col-md-2">
                    <input type="email" class="form-control" name="email" placeholder="example@examp.com"/>
                </div>
            </div>
            <div class="col-md-12" style="padding-top: 2%;">
                <div class="col-md-offset-4 col-md-2">
                    <label>Password</label>
                </div>
                <div class="col-md-2">
                    <input type="password" class="form-control" name="password" placeholder="give the password"/>
                </div>
            </div>
            <div class="col-md-12" style="padding-top: 2%;">
                <div class="col-md-offset-4 col-md-2">
                    <label>Re enter the Password</label>
                </div>
                <div class="col-md-2">
                    <input type="password" class="form-control" name="repassword" placeholder="give the password again"/>
                </div>
            </div>
            <div class="col-md-12" style="padding-top: 2%;">
                <div class="col-md-offset-4 col-md-4">
                    <input type="submit" class="btn btn-primary btn-lg btn-block" value="Submit" />
                </div>
            </div>
            
        </form>
        <div class="col-md-12" style="padding-top: 2%;">
                <div class="col-md-offset-4 col-md-4">
                    <a href="login.php"><input type="submit" class="btn btn-primary btn-lg btn-block" value="Already Registerd?" /></a>
                </div>
            </div>
    </body>
</html>

